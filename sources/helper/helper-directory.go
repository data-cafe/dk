/*
 * data·café
 * Copyright (C) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

package helper

import (
	"os"
)

/**
 * Create directory if not exists
 */
func CreateDirectoryIfNotExists(directory string) error {
	if DirectoryNotExists(directory) {
		err := os.Mkdir(directory, 0755)
		if err != nil {
			return err
		}
	}
	return nil
}

/**
 * Check if a folder (or a file) doesn't exist
 */
func DirectoryNotExists(path string) bool {
	_, err := os.Stat(path)
	return os.IsNotExist(err)
}

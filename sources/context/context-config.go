/*
 * data·café
 * Copyright (C) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

package context

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

// -- Type definitions ---------------------------------------------------------

type TypeConfig struct {
	Version int           `json:"version"`
	Name    string        `json:"name"`
	Slug    string        `json:"slug"`
	Ide     TypeConfigIde `json:"ide"`
	Git     TypeConfigGit `json:"git"`
	K8S     TypeConfigK8s `json:"k8s"`
}

type TypeConfigIde struct {
	List []string `json:"list"`
}

type TypeConfigGit struct {
	Remote string `json:"remote"`
}

type TypeConfigK8s struct {
	Context    string   `json:"context"`
	Namespaces []string `json:"namespaces"`
	Options    []string `json:"options"`
}

// -- Singleton ----------------------------------------------------------------

var config *TypeConfig

func Config() TypeConfig {
	if config == nil {
		Init()
	}
	return *config
}

// -- Helper -------------------------------------------------------------------

// Save the config as <directory>/.dk/workspace.json
// Note: ensure that the directory exists before calling this function
func PersistConfig() error {
	// To JSON
	byteValue, err := json.MarshalIndent(Config(), "", "  ")
	if err != nil {
		fmt.Printf("11: %#v\n", err)
		return err
	}

	// Persist file
	err = ioutil.WriteFile(configFile, byteValue, 0644)
	if err != nil {
		fmt.Printf("11: %#v\n", err)
		return err
	}

	return nil
}

// Load the config as <directory>/.dk/workspace.json
// Note: ensure that the directory exists before calling this function
func LoadConfig(configFile string) (config TypeConfig, err error) {

	// Read file
	byteValue, err := ioutil.ReadFile(configFile)
	if err != nil {
		return
	}

	// Parse file
	err = json.Unmarshal(byteValue, &config)
	if err != nil {
		return
	}

	return
}

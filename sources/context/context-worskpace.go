/*
 * data·café
 * Copyright (C) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

package context

// -- directory ----------------------------------------------------------------

var directory string

// Path of the current workspace directory
func Directory() string {
	if directory == "" {
		Init()
	}
	return directory
}

// -- DK directory -------------------------------------------------------------

var dkDirectory string

// Path of dk folder in current workspace directory
func DkDirectory() string {
	if dkDirectory == "" {
		Init()
	}
	return dkDirectory
}

// -- DK directory -------------------------------------------------------------

// Path of dk config file in current workspace directory
// Private property (yet no need outside)
var configFile string

/*
 * data·café
 * Copyright (C) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

package context

import (
	"strconv"
	"time"
)

const DK_VERSION = 1

const DK_DIRECTORY = ".dk"
const DK_CONFIG_FILE = "workspace.json"

// This should be a constant
var GIT_BRANCHES = [...]string{"main", "develop"}

// This should be a constant
var GIT_INITIAL_BRANCH = "dk-init-" + strconv.Itoa(time.Now().UTC().Nanosecond())

// These all should be constant
var K8S_NAMESPACES = [...]string{"default", "dev", "staging", "test", "preprod", "prod"}
var K8S_DEFAULT_NAMESPACES = [...]string{"test", "prod"}
var K8S_OPTIONS = [...]string{"metrics"}
var K8S_DEFAULT_OPTIONS = [...]string{"metrics"}

// These all should be constant
var IDE_LIST = [...]string{"code", "intellij"}
var IDE_DEFAULT_LIST = [...]string{"code"}

// These all should be constant
var IGNORE_FILE_DEFAULT = [...]string{"windows", "linux", "macos"}

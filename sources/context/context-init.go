/*
 * data·café
 * Copyright (C) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

package context

import (
	"os"
)

func Init() error {
	// Retreive the current directory
	dir, err := os.Getwd()
	if err != nil {
		return err
	}

	// Fill Directories
	directory = dir
	dkDirectory = dir + "/" + DK_DIRECTORY
	configFile = dir + "/" + DK_DIRECTORY + "/" + DK_CONFIG_FILE

	// Load config from file
	conf, err := LoadConfig(configFile)
	if err != nil {
		return err
	}

	// Fill Config
	config = &conf

	return nil
}

// Set the new context
// I.e. when the user create a new workspace
func InitFromQuestionnaire(conf TypeConfig) error {

	// Fill Directories
	directory = "./" + conf.Slug
	dkDirectory = directory + "/" + DK_DIRECTORY
	configFile = dkDirectory + "/" + DK_CONFIG_FILE

	// Fill Config
	config = &conf

	return nil
}

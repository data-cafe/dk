/*
 * data·café
 * Copyright (C) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

package context

import "github.com/spf13/cobra"

const FLAG_IGNORE_GIT = "ignore-git"

type Flags struct {
	IgnoreGit bool // flag --ignore-git
}

func GetFlags(cmd *cobra.Command) Flags {
	return Flags{
		IgnoreGit: GetFlag(cmd, FLAG_IGNORE_GIT),
	}
}

func GetFlag(cmd *cobra.Command, flag string) bool {
	val, err := cmd.Flags().GetBool(flag)
	return err == nil && val
}

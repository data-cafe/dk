/*
 * data·café
 * Copyright (C) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"data-cafe.io/dk/context"
	"data-cafe.io/dk/kube"
	"data-cafe.io/dk/populate"
	"data-cafe.io/dk/questionnaire"
	"data-cafe.io/dk/versionning"
)

// newCmd represents the new command
var newCmd = &cobra.Command{
	Use:   "new",
	Short: "Create a new data•cafe workspace",
	Long: `
This tool will automatically create a new data•café workspace from an empty git
repository and by adding the minimum configuration needed and chore initial taks.

This will:
- git clone your repo (not yet portable g#142)
- commit all following changes in a dedicated branch
- config: persist the dk config file
- k8s: choose namespaces
- ... more to come

This is based on Cobra CLI.`,
	Run: RunNew,
}

func init() {
	workspaceCmd.AddCommand(newCmd)

	// Flag: --ignore-git
	newCmd.PersistentFlags().Bool(context.FLAG_IGNORE_GIT, false, "Ignore the git part (repo clone, commit, ...)")
}

// SimpleFunction prints a message
func RunNew(cmd *cobra.Command, args []string) {
	flags := context.GetFlags(cmd)
	printResult(nil, "flags retrieved", false)

	contexts, err := kube.ListContextsFromHome()
	printResult(err, "k8s contexts retrieved", true)
	fmt.Println()

	// Ask info from user
	answers, err := questionnaire.Ask(flags, contexts)
	fmt.Println("") // separate processing from questionnaire
	printResult(
		err,
		"questionnaire is correctly answered",
		true,
	)

	// Create context
	printResult(
		context.InitFromQuestionnaire(questionnaire.ToConfig(answers)),
		"create dk context from your answers",
		true,
	)

	// Init git (and create directory)
	if !flags.IgnoreGit {
		printResult(
			versionning.Init(),
			"git repository initialisation",
			true,
		)
	}

	// Create workspace
	printResult(
		populate.CreateWorkspaceDirectories(),
		"create workspace directories",
		true,
	)

	// Persist config
	printResult(
		context.PersistConfig(),
		"persist workspace config",
		true,
	)

	// Get ressources
	tempFolder, ressourcesFolder, err := populate.DownloadRessources()
	printResult(err, "Download ressources", true)
	defer os.RemoveAll(tempFolder)

	// Population
	printResult(populate.IgnoreFile(ressourcesFolder), "create .gitignore", true)
	printResult(populate.PopulateIDEs(ressourcesFolder), "create IDE settings", true)

	// Init k8s
	// TODO g#144

	// Create MR with all this init
	if !flags.IgnoreGit {
		printResult(
			versionning.CreateMR(),
			"commit to git",
			true,
		)
		fmt.Printf(
			"\n\x1b[31;1mℹ️\x1b[0m git: don't forget to merge the branch \"%s\" to main\n\n",
			context.GIT_INITIAL_BRANCH,
		)
	}
}

// -- UI -----------------------------------------------------------------------

func printResult(err error, message string, exitOnError bool) {
	if err != nil {
		fmt.Println("\x1b[1;31m×\x1b[0m", message)
		fmt.Printf("\x1b[31;1m%s\x1b[0m\n", fmt.Sprintf("error: %s", err))
		if exitOnError {
			os.Exit(1)
		}
	} else {
		fmt.Println("\x1b[1;34m✓\x1b[0m", message)
	}
}

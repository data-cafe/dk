/*
 * data·café
 * Copyright (C) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

package questionnaire

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"github.com/AlecAivazis/survey/v2"
)

// This should be a constant
var AnswserTrimmer = survey.TransformString(strings.TrimSpace)

func AnswserTrimmedRequired(val interface{}) error {
	if str, ok := val.(string); ok {
		// if the string is shorter than the given value
		if len([]rune(strings.TrimSpace(str))) < 1 {
			// yell loudly
			return errors.New("Value is required")
		}
	} else {
		// otherwise we cannot convert the value into a string and cannot enforce length
		return fmt.Errorf("cannot enforce length on response of type %v", reflect.TypeOf(val).Name())
	}

	// the input is fine
	return nil
}

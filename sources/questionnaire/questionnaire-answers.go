/*
 * data·café
 * Copyright (C) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

package questionnaire

import (
	"strings"

	"data-cafe.io/dk/context"
	"github.com/gosimple/slug"
)

type Answers struct {
	Name          string
	Git           string
	IdeList       []string `survey:"ide_list"`
	K8sContext    string   `survey:"k8s_context"`
	K8sOptions    []string `survey:"k8s_options"`
	K8sNamespaces []string `survey:"k8s_namespaces"`
}

/**
 * Convert the answers to a config
 */
func ToConfig(answers Answers) context.TypeConfig {
	// Special treatment for K8S Context
	cntxt := ""
	if answers.K8sContext != "none" {
		// extract context name from: "name (file)"
		cntxt = strings.Split(answers.K8sContext, " ")[0]
	}

	return context.TypeConfig{
		Version: context.DK_VERSION,
		Name:    answers.Name,
		Slug:    slug.Make(answers.Name),
		Git: context.TypeConfigGit{
			Remote: answers.Git,
		},
		Ide: context.TypeConfigIde{
			List: answers.IdeList,
		},
		K8S: context.TypeConfigK8s{
			Context:    cntxt,
			Namespaces: answers.K8sNamespaces,
			Options:    answers.K8sOptions,
		},
	}
}

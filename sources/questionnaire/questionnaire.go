/*
 * data·café
 * Copyright (C) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

package questionnaire

import (
	"fmt"

	"data-cafe.io/dk/context"
	"data-cafe.io/dk/kube"
	"github.com/AlecAivazis/survey/v2"
)

func Ask(flags context.Flags, contexts []kube.AvailableContext) (answers Answers, err error) {
	answers = Answers{}

	// kube contexts
	cntxts := []string{}
	for _, c := range contexts {
		cntxts = append(cntxts, fmt.Sprintf("%s (%s)", c.Name, c.FileLink))
	}
	cntxts = append(cntxts, "none")

	// Git URL validator
	var gitValidator survey.Validator
	if flags.IgnoreGit {
		gitValidator = nil
	} else {
		gitValidator = AnswserTrimmedRequired
	}

	var qs = []*survey.Question{
		{
			Name:      "name",
			Prompt:    &survey.Input{Message: "Workspace's name"},
			Validate:  AnswserTrimmedRequired,
			Transform: AnswserTrimmer,
		}, {
			Name:      "git",
			Prompt:    &survey.Input{Message: "Enter Git url.git"},
			Validate:  gitValidator,
			Transform: AnswserTrimmer,
		}, {
			Name: "k8s_context",
			Prompt: &survey.Select{
				Message: "Which K8S cluster to deploy to?",
				Options: cntxts,
			},
			Validate: survey.Required,
		}, {
			Name: "k8s_namespaces",
			Prompt: &survey.MultiSelect{
				Message: "Which K8S namespaces do you want to use?",
				Options: context.K8S_NAMESPACES[:],
				Default: context.K8S_DEFAULT_NAMESPACES[:],
			},
			Validate: survey.Required,
		}, {
			Name: "k8s_options",
			Prompt: &survey.MultiSelect{
				Message: "Which K8S options do you want to use?",
				Options: context.K8S_OPTIONS[:],
				Default: context.K8S_DEFAULT_OPTIONS[:],
			},
			Validate: survey.Required,
		}, {
			Name: "ide_list",
			Prompt: &survey.MultiSelect{
				Message: "Which IDE do you want to use?",
				Options: context.IDE_LIST[:],
				Default: context.IDE_DEFAULT_LIST[:],
			},
			Validate: survey.Required,
		},
	}

	// Launch the questionnaire
	err = survey.Ask(qs, &answers)

	// Auto-return values
	return
}

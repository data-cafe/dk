/*
 * data·café
 * Copyright (C) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

package versionning

import (
	"fmt"
	"os/exec"

	"data-cafe.io/dk/context"
	"github.com/go-git/go-git/v5"
)

func Init() error {

	// Init git
	// TODO: use go-git but, yet, it doesn't support SSL verification for
	//		 private repos
	cmd := exec.Command("git", "clone", context.Config().Git.Remote, context.Config().Slug)
	cmd.Dir = "." // need to clone from the directory parent
	err := cmd.Run()
	if err != nil {
		fmt.Printf("10: %#v\n", cmd)
		fmt.Printf("10: %#v\n", err)
		return err
	}

	// TODO: create woking branches (develop, ...)
	// @see GIT_BRANCHES
	// @see GetGitBranchesToCreate(repo)

	// Git create init branch
	// TODO: use go-git but
	//		 see: https://github.com/go-git/go-git/blob/master/_examples/branch/main.go
	//		 see: https://stackoverflow.com/a/69988054/8886975
	cmd = exec.Command("git", "checkout", "-b", context.GIT_INITIAL_BRANCH)
	cmd.Dir = context.Config().Slug
	err = cmd.Run()
	if err != nil {
		fmt.Printf("11: %#v\n", cmd)
		fmt.Printf("11: %#v\n", err)
		return err
	}

	return nil
}

func CreateMR() error {
	// Open repo
	repo, err := git.PlainOpen(context.Directory())
	if err != nil {
		return err
	}

	// Get repo index
	wt, err := repo.Worktree()
	if err != nil {
		return err
	}

	// Add all files
	_, err = wt.Add(".")
	if err != nil {
		return err
	}

	// Create commit
	commit, err := wt.Commit("chore: initial dk settings", &git.CommitOptions{})
	if err != nil {
		return err
	}

	// Append commit
	_, err = repo.CommitObject(commit)
	if err != nil {
		return err
	}

	// Git push branch
	// TODO: use go-git
	cmd := exec.Command("git", "push", "-u", "origin", context.GIT_INITIAL_BRANCH)
	cmd.Dir = context.Config().Slug
	err = cmd.Run()
	if err != nil {
		return err
	}

	return nil
}

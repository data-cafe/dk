/*
 * data·café
 * Copyright (C) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

package kube

import (
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/kubernetes-client/go-base/config/api"

	"gopkg.in/yaml.v3"
)

type AvailableContext struct {
	Name     string
	Cluster  string
	User     string
	FileName string
	FileLink string
	FilePath string
}

func ListContextsFromHome() (result []AvailableContext, err error) {
	// Get Home path
	dirname, err := os.UserHomeDir()
	if err != nil {
		return
	}

	// List files in ~/.kube/
	files, err := ioutil.ReadDir(dirname + "/.kube")
	if err != nil {
		return
	}

	// Prepare result
	result = []AvailableContext{}

	// Look into each file
	for _, file := range files {
		// Pre-requisite: file (not dir)
		if file.IsDir() {
			continue
		}

		// Load file
		yamlFile, err := ioutil.ReadFile(filepath.Join(dirname, ".kube", file.Name()))
		if err != nil {
			continue
		}

		// Parse as a Kube Config YAML
		// we're using an existing implementation of kube config
		// https://github.com/kubernetes-client/go-base/blob/master/config/api/types.go
		// note: this is supposed to be also used by client-go
		c := &api.Config{}
		err = yaml.Unmarshal(yamlFile, c)
		if err != nil {
			continue
		}

		// List all contexts
		// each line is: "<context-name> (<file-path>)"
		for _, context := range c.Contexts {
			result = append(result, AvailableContext{
				Name:     context.Name,
				Cluster:  context.Context.Cluster,
				User:     context.Context.AuthInfo,
				FileName: file.Name(),
				FileLink: filepath.Join("~", ".kube", file.Name()),
				FilePath: filepath.Join(dirname, ".kube", file.Name()),
			})
		}
	}

	return
}

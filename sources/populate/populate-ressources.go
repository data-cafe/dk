/*
 * data·café
 * Copyright (C) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

package populate

import (
	"os"

	"data-cafe.io/dk/helper"
	"github.com/artdarek/go-unzip/pkg/unzip"
)

/**
 * Download dk ressources and return the path to the folder ressources.
 * I.e.: /tmp/ressources3660681975/dk-main-ressources/ressources
 */
func DownloadRessources() (string, string, error) {

	// Create temporary folder to download zips
	tempFolderName, err := os.MkdirTemp("", "ressources")
	if err != nil {
		return "", "", err
	}

	// Build paths
	url := "https://gitlab.com/data-cafe/dk/-/archive/main/dk-main.zip?path=ressources"
	path := tempFolderName + "/ressources.zip"

	// Download archive
	err = helper.DownloadFile(path, url)
	if err != nil {
		return "", "", err
	}

	// Unzip archive
	_, err = unzip.New().Extract(path, tempFolderName)
	if err != nil {
		return "", "", err
	}

	return tempFolderName, tempFolderName + "/dk-main-ressources/ressources", nil
}

/*
 * data·café
 * Copyright (C) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

package populate

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"data-cafe.io/dk/context"
)

func IgnoreFile(ressourcesFolder string) error {
	// Download .gitignore via Toptal generator
	destFile := context.Directory() + "/.gitignore"
	ressourcePath := ressourcesFolder + "/gitignore/.gitignore"
	generatedURL := "https://www.toptal.com/developers/gitignore/api/" + getIgnoreFileParams()

	// Get (1) the ressource
	ressourceBuffer, err := ioutil.ReadFile(ressourcePath)
	if err != nil {
		return err
	}

	// Get (2) the generated
	resp, err := http.Get(generatedURL)
	if err != nil {
		return err
	}
	generatedBuffer, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(destFile)
	if err != nil {
		return err
	}
	defer out.Close()

	// Merge (1) the ressource and (2) the generated
	var mergedBuffer bytes.Buffer
	mergedBuffer.Write(ressourceBuffer)
	mergedBuffer.Write(generatedBuffer)

	// Write the file
	return ioutil.WriteFile(destFile, mergedBuffer.Bytes(), 0644)
}

func getIgnoreFileParams() string {
	// Default
	chain := strings.Join(context.IGNORE_FILE_DEFAULT[:], ",")

	// Ide
	for _, ide := range context.Config().Ide.List {
		switch ide {
		case "code":
			chain += ",visualstudiocode"
		case "intellij":
			// do nothing: the intellij ressource has its own .gitignore
			// otherwise chain += ",intellij+iml"
		}
	}

	return chain
}

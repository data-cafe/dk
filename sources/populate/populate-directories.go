/*
 * data·café
 * Copyright (C) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

package populate

import (
	"fmt"

	"data-cafe.io/dk/context"
	"data-cafe.io/dk/helper"
)

func CreateWorkspaceDirectories() error {
	// Create directory (if not created by git)
	err := helper.CreateDirectoryIfNotExists(context.Directory())
	if err != nil {
		fmt.Printf("11: %#v\n", err)
		return err
	}

	// Create our directory
	err = helper.CreateDirectoryIfNotExists(context.DkDirectory())
	if err != nil {
		fmt.Printf("11: %#v\n", err)
		return err
	}

	return nil
}

/*
 * data·café
 * Copyright (C) 2022 Data Terrae
 * This program is under the terms of the GNU Affero General Public License version 3
 * The full license information can be found in LICENSE in the root directory of this project.
 */

package populate

import (
	cp "github.com/otiai10/copy"

	"data-cafe.io/dk/context"
)

func PopulateIDEs(ressourcesFolder string) error {

	// Populate each IDE
	for _, ide := range context.Config().Ide.List {
		err := populateIDE(ressourcesFolder, ide)
		if err != nil {
			return err
		}
	}

	return nil
}

func populateIDE(ressourcesFolder string, ide string) error {

	// Build paths
	path := ressourcesFolder + "/" + ide

	err := cp.Copy(path, context.Directory())
	if err != nil {
		return err
	}

	return nil
}

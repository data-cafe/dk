# Contribution

## Install dev env

### Install `go`

First, install go, using their website [go.dev/doc/install](https://go.dev/doc/install) or, for debian like:

```sh
sudo add-apt-repository ppa:longsleep/golang-backports
sudo apt install golang-go
```

Check the version:

```sh
go --version
```

###
